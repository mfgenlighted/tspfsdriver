﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PFSDriver
{
    public partial class GetSigninInfo : Form
    {
        public string User;
        public string Password;
        public string OperationCode;
        public string WorkCenter;

        public GetSigninInfo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            User = tbUser.Text;
            Password = tbPassword.Text;
            OperationCode = tbOperationCode.Text;
            WorkCenter = tbWorkCenter.Text;
            DialogResult = DialogResult.OK;
        }

        private void GetSigninInfo_Load(object sender, EventArgs e)
        {

        }
    }
}
