﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PFSDriver
{
    public partial class EditPFSConfig : Form
    {
        public string Server;
        public string DatabaseName;
        public string OpCode;

        public EditPFSConfig(string server, string database, string opcode)
        {
            InitializeComponent();
            Server = server;
            DatabaseName = database;
            OpCode = opcode;
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            Server = tbServer.Text;
            DatabaseName = tbDatabase.Text;
            OpCode = tbOpCode.Text;
            DialogResult = DialogResult.OK;
        }
    }
}
